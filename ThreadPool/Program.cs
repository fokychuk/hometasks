﻿using System;
using System.Diagnostics;
using System.Threading;

namespace ThreadPool
{
	static class Program
	{
		static volatile bool complete;
		static void Main(string[] args)
		{
			//			int concurrencyLevel = 2;
			//			Process.GetCurrentProcess().ProcessorAffinity = new IntPtr((1 << concurrencyLevel) - 1);

			Console.WriteLine("\n----------=======LockAndWSQ ThreadPool tests=======----------");
			var testsWS = new LockAndWsqThreadPoolWrapperTests();
			testsWS.LongCalculations();
			testsWS.ShortCalculations();
			testsWS.ExtremelyShortCalculations();
			testsWS.InnerShortCalculations();
			testsWS.InnerExtremelyShortCalculations();

			Console.WriteLine("\n----------=======SimpleLock ThreadPool tests=======----------");
			var testsSL = new SimpleLockThreadPoolWrapperTests();
			testsSL.LongCalculations();
			testsSL.ShortCalculations();
			testsSL.ExtremelyShortCalculations();
			testsSL.InnerShortCalculations();
			testsSL.InnerExtremelyShortCalculations();
			//
			Console.WriteLine("\n----------=======DotNet ThreadPool Tasks tests=======----------");
			var testsDNT = new DotNetThreadPoolTaskWrapperTests();
			testsDNT.LongCalculations();
			testsDNT.ShortCalculations();
			testsDNT.ExtremelyShortCalculations();
			testsDNT.InnerShortCalculations();
			testsDNT.InnerExtremelyShortCalculations();

			Console.WriteLine("\n----------=======DotNet ThreadPool tests=======----------");
			var testsDN = new DotNetThreadPoolWrapperTests();
			testsDN.LongCalculations();
			testsDN.ShortCalculations();
			testsDN.ExtremelyShortCalculations();
			testsDN.InnerShortCalculations();
			testsDN.InnerExtremelyShortCalculations();
		}
	}
}
